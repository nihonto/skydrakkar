package autozvit.service.geo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
//import java.lang.Double;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
//import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
//import javax.xml.parsers.SAXParser;
public class GeocoderByAddress/* extends Thread*/
{
  public GeocoderByAddress(){}
  //debug 0-<no debug> 1-<start debug data> 2-<with json data parsing> 3-<with google answer>
  public String start(String username,String password,String database,int data_wait_count,int data_wait_timeout,int debug)
  {
    Connection connection=null;
    String ret_val=debug==0?"->":"debug="+debug,str_data,str1=null,str2=null,str3=null,str4=null,str5=null;
    String driver="oracle.jdbc.driver.OracleDriver",jdbc_oracle="jdbc:oracle:thin:@";
    String address=null;
    byte[] data=null;
    PreparedStatement prestmt=null;
    Statement stmt=null;
    ResultSet rset=null;
    //connect to Oracle database (by username,password,database)
    //seek tables org,stud,spec.
    //find adddresses by this tables.
    //send http request to google.com by address from this tables
    //receive http response from google.com with geolocation position
    //save data in Oracle database
    try{
      Class.forName(driver);  
      connection=DriverManager.getConnection(jdbc_oracle+database,username,password);
/*
SELECT distinct nvl(o.region_name,r.name) as region_name,o.distr_name,o.city_name,o.street_name,o.build_num FROM org o,region r
WHERE o.region_code=r.region_code(+) AND o.city_name IS NOT NULL AND rownum<2 and NOT EXISTS(
SELECT 1 FROM geo_object go
WHERE NVL(go.region_name,'*')=NVL(nvl(o.region_name,r.name),'*') AND NVL(go.distr_name,'*')=NVL(o.distr_name,'*')
AND go.city_name=o.city_name
AND NVL(go.street_name,'*')=NVL(o.street_name,'*') AND NVL(go.build_num,'*')=NVL(o.build_num,'*'))
 */      
      String url="http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=ru&address=";
      String current_sql=null,current_url=null;
      //random function for sql number case  -> Date().getSeconds();
      Date d;
      String sel_1="SELECT distinct nvl(o.region_name,r.name) as region_name,o.distr_name,o.city_name,o.street_name,o.build_num FROM org o,region r "+
                   "WHERE o.region_code=r.region_code(+) AND o.city_name IS NOT NULL AND rownum<2 and NOT EXISTS("+
                   "SELECT 1 FROM geo_object go "+
                   "WHERE NVL(go.region_name,'*')=NVL(nvl(o.region_name,r.name),'*') AND NVL(go.distr_name,'*')=NVL(o.distr_name,'*') "+
                   "AND go.city_name=o.city_name "+
                   "AND NVL(go.street_name,'*')=NVL(o.street_name,'*') AND NVL(go.build_num,'*')=NVL(o.build_num,'*'))";
      String sel_2="SELECT distinct o.region_name,o.distr_name,o.city_name,o.street_name,o.build_num FROM org_item o "+
                   "WHERE o.city_name IS NOT NULL AND rownum<2 and NOT EXISTS("+
                   "SELECT 1 FROM geo_object go "+
                   "WHERE NVL(go.region_name,'*')=NVL(o.region_name,'*') AND NVL(go.distr_name,'*')=NVL(o.distr_name,'*') "+
                   "AND go.city_name=o.city_name "+
                   "AND NVL(go.street_name,'*')=NVL(o.street_name,'*') AND NVL(go.build_num,'*')=NVL(o.build_num,'*'))";
      String sel_3="SELECT distinct nvl(o.region_name,r.name) as region_name,o.distr_name,o.city_name,o.street_name,o.build_num FROM spec o,region r "+
                   "WHERE o.region_code=r.region_code(+) AND o.city_name IS NOT NULL AND rownum<2 and NOT EXISTS("+
                   "SELECT 1 FROM geo_object go "+
                   "WHERE NVL(go.region_name,'*')=NVL(nvl(o.region_name,r.name),'*') AND NVL(go.distr_name,'*')=NVL(o.distr_name,'*') "+
                   "AND go.city_name=o.city_name "+
                   "AND NVL(go.street_name,'*')=NVL(o.street_name,'*') AND NVL(go.build_num,'*')=NVL(o.build_num,'*'))";
      String sel_4="SELECT distinct nvl(o.region_name,r.name) as region_name,o.distr_name,o.city_name,o.street_name,o.build_num FROM stud o,region r "+
                   "WHERE o.region_code=r.region_code(+) AND o.city_name IS NOT NULL AND rownum<2 and NOT EXISTS("+
                   "SELECT 1 FROM geo_object go "+
                   "WHERE NVL(go.region_name,'*')=NVL(nvl(o.region_name,r.name),'*') AND NVL(go.distr_name,'*')=NVL(o.distr_name,'*') "+
                   "AND go.city_name=o.city_name "+
                   "AND NVL(go.street_name,'*')=NVL(o.street_name,'*') AND NVL(go.build_num,'*')=NVL(o.build_num,'*'))";
      String ins_1="INSERT INTO geo_object(region_name,distr_name,city_name,street_name,build_num,geo_name,geo_lat,geo_lng) VALUES(?,?,?,?,?,?,?,?)";
      int sql_num;
      try{
        d=new Date();
        //sql_num=(int)(d.getSeconds()/10);//(../15) up case to sel_3,sel_4
        sql_num=(int)(d.getSeconds());
        if(debug>0)ret_val+="|sql_number="+sql_num;//debug
        if(sql_num<2)current_sql=sel_1;//up up ^ to sel_3,sel_4
        else if(sql_num>=2&&sql_num<5)current_sql=sel_2;
        else if(sql_num>=5&&sql_num<10)current_sql=sel_3;
        else if(sql_num>=10&&sql_num<60)current_sql=sel_4;
        /*switch(sql_num){
          case 0:current_sql=sel_1;break;
          case 1:current_sql=sel_2;break;
          case 2:current_sql=sel_3;break;
          case 3:current_sql=sel_4;break;
          case 4:current_sql=sel_3;break;
          case 5:current_sql=sel_4;
        }*/
        stmt=connection.createStatement();
        rset=stmt.executeQuery(current_sql);
        if(rset.next()){
          str1=rset.getString(1);
          str2=rset.getString(2);
          str3=rset.getString(3);
          str4=rset.getString(4);
          str5=rset.getString(5);
          address="Україна";
          if(str1!=null&&str1.length()>0){//region_name
            if(str1.startsWith("Київ")){if(!str3.equals("Київ"))address+=","+str1;}//region='Kiev...' city=no'Kiev'
            else if(str1.equals("Севастополь")){if(!str3.equals("Севастополь"))address+=","+str1;}
            else address+=","+str1+" область";
          }
          if(str2!=null&&str2.length()>0){//distr_name
            if(str2.equals("Ялта"))address+=",Ялтинский горсовет";//Ялтинский горсовет
            else if(str2.equals("Алушта"))address+=",Алуштинский горсовет";//Алуштинский горсовет
            else if(str2.equals("Судак"))address+=",Судакский горсовет";//Судакский горсовет
            else if(str2.endsWith("кий"))address+=","+str2+" район";
          }
          if(str3!=null&&str3.length()>0){//city_name
            int i=str3.lastIndexOf('.');//kill the substring (м.Севастополь)
            if(i>-1&&i<str3.length()-1){//point present and not ends
              address+=","+str3.substring(i+1);
            }address+=","+str3;
          }
          if(str4!=null&&str4.length()>0){//street_name
            int i=str4.lastIndexOf('.');//kill the substring (І.Лепсе, Ген.Острякова, П.Д.Романова)
            if(i>-1&&i<str4.length()-1){//point present and not ends
              address+=","+str4.substring(i+1);
            }else address+=","+str4;
          }
          if(str5!=null&&str5.length()>0){address+=","+str5;}//build_num
        }
        while(true){
          if(address!=null){
            if(debug>0)ret_val+="|get address from database="+address;//debug
            current_url=url+URLEncoder.encode(address,"UTF-8");
            if(debug>0)ret_val+="|url to find location="+current_url;//debug
            data=this.getPage(current_url,data_wait_count,data_wait_timeout);
          }
          if(data!=null){
            str_data=new String(data);
            if(debug>0)ret_val+="|get location from google"+(debug>2?"="+str_data:"");//debug
/*XML     
<GeocodeResponse>
 <status>OK</status>
 <result>
  <type>street_address</type>
  <formatted_address>1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA</formatted_address>
  ...
  <geometry>
   <location>
    <lat>37.4217550</lat>
    <lng>-122.0846330</lng>
   </location>
   ...
  </geometry>
 </result>
</GeocodeResponse>    
*/ 
/*JSON
{
  "results" : [
  {  
      "address_components" :[...],
      "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",
      "geometry" : {
        "location" : {
          "lat" : 37.42291810,
          "lng" : -122.08542120
        },
        ...
      },
      "types" : [ "street_address" ]
  }
  ],
  "status" : "OK"
}
*/
            //data parsing
            JSONParser parser=new JSONParser();
            Object obj=parser.parse(str_data);
            if(debug>1)ret_val+="|parsed json data";//debug
            JSONObject jobj=(JSONObject)obj;
            if(debug>1)ret_val+="|cast to json object";//debug
            //JSONArray array=(JSONArray)obj;
            //java.util.List->array
            //java.util.Map->object
            String status=(String)jobj.get("status");
            if(debug>1)ret_val+="|get status";//debug
            if(status!=null&&status.equals("OK")){
              if(debug>1)ret_val+="|status="+status;//debug
              JSONArray results=(JSONArray)jobj.get("results");
              if(debug>1)ret_val+="|get results";//debug
              JSONObject results_data=(JSONObject)results.get(0),geometry;
              if(results_data!=null){
                String formatted_address;//utf
                //Double lat_val,lng_val;
                String addr=null,lat=null,lng=null;
                if(debug>1)ret_val+="|get results{data}";//debug
                //JSONArray address_components=(JSONArray)results_data.get("address_components");
                //for(int i=0;i<address_components.size();i++)...=address_components.get(i);
                formatted_address=(String)results_data.get("formatted_address");
                geometry=(JSONObject)results_data.get("geometry");
                if(formatted_address!=null)addr=new String(formatted_address.getBytes(/*Cp1251*/),"UTF-8");
                if(geometry!=null){
                  if(debug>1)ret_val+="|get geometry";//debug
                  JSONObject location=(JSONObject)geometry.get("location");
                  if(location!=null){
                    if(debug>1)ret_val+="|get location{data}";//debug
                    //lat_val=(Double)location.get("lat");
                    //lng_val=(Double)location.get("lng");
                    //lat=lat_val.toString();
                    //lng=lng_val.toString();
                    lat=location.get("lat").toString();
                    lng=location.get("lng").toString();
                  }
                }
                //insert to database
                if(lat!=null&&lng!=null){
                  if(debug>0)ret_val+="|formatted_address="+addr+"|lat="+lat+"|lng="+lng;//debug
                  current_sql=ins_1;
                  prestmt=connection.prepareStatement(current_sql);
                  prestmt.setString(1,str1);
                  prestmt.setString(2,str2);
                  prestmt.setString(3,str3);
                  prestmt.setString(4,str4);
                  prestmt.setString(5,str5);
                  prestmt.setString(6,addr);
                  prestmt.setString(7,lat);
                  prestmt.setString(8,lng);
                  prestmt.executeQuery();
                  if(debug>0)ret_val+="|location inserted into database";//debug
                  else ret_val+="|address="+address+"|formatted_address="+addr+"|lat="+lat+"|lng="+lng;
                  break;//get answer(stop function)
                }
              }
            }//if status==OK
            else if(status!=null&&status.equals("ZERO_RESULTS")){//if status==ZERO_RESULTS
              if(debug>0)ret_val+="|status="+status;//debug
              //try again
              int i=address.lastIndexOf(",");
              if(i>-1)address=address.substring(0,i);
              else/*put info here*/break;//address failed(stop function) may be insert in geo_object as failed address ? 
            }
            else{//other status
              if(debug>0)ret_val+="|status="+status;//debug
              else ret_val+="|address="+address+"|location status="+status;break;//bad answer(stop function)
            }
          }//if data!=null
          else{ret_val+="|address="+address+"|data not found";break;}//data failed(stop function)
        }//while(true)
      }catch(SQLException sql_e){ret_val+="|sql error="+current_sql+"|"+sql_e.getLocalizedMessage();}
      try{if(rset!=null)rset.close();rset=null;if(prestmt!=null)prestmt.close();prestmt=null;if(stmt!=null)stmt.close();stmt=null;}catch(SQLException sql_e){ret_val+="|"+sql_e.getLocalizedMessage();}
      if(connection!=null){connection.close();connection=null;}
    }catch(Exception e){ret_val+="|"+e.getLocalizedMessage();}
    return ret_val;
  }
  //version use tesis that content_length == -1
  private byte[] getPage(String page_url,int data_wait_count,int data_wait_timeout)/*throws Exception*/
  {
    byte[] page=null;
    int length,content_length,recv_length=0,count=-1,buffer_size=1024;
    byte[] buffer=new byte[buffer_size];
    try{
      URL url=new URL(page_url);
      try{
        InputStream connection_stream;
        ByteArrayOutputStream data_stream=null;
        HttpURLConnection http_connection;
        http_connection=(HttpURLConnection)url.openConnection();
        http_connection.setRequestProperty("User-Agent","GeocoderByAddressService");
        http_connection.connect();
        content_length=http_connection.getContentLength();
        if(http_connection.getResponseCode()>=200&&http_connection.getResponseCode()<300){
          connection_stream=http_connection.getInputStream();
          data_stream=new ByteArrayOutputStream();
          do{
            length=connection_stream.available();            
            if(length>0){//data ready
              length=connection_stream.read(buffer);
              data_stream.write(buffer,0,length);
              recv_length+=length;
            }
            else{//no data! panic!
              if(++count==data_wait_count)break;//count try again is up
              try{Thread.sleep(data_wait_timeout);}catch(InterruptedException i_e){}//waiting for data (sleeping sun)
            }
            if(content_length!=-1&&recv_length>=content_length)break;//use length if positive
          }while(true);
          if(recv_length>0)page=data_stream.toByteArray();//if data present use it
        }//if 
        http_connection.disconnect();http_connection=null;
        if(data_stream!=null){data_stream.close();data_stream=null;}
      }catch(IOException io_e){}
    }catch(MalformedURLException mu_e){}
    buffer=null;
    return page;
  }
}