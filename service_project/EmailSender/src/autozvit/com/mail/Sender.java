package autozvit.com.mail;

import java.util.Properties;
import java.util.Date;
import javax.mail.*;
import javax.mail.internet.*;
import com.sun.mail.smtp.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/*
    EmailSender versions
    --------------------
    NO VERSION - only smtps support
    VERSION 1  - smtps/smtp proto support (secure/no_secure smtp proto)
*/

public class Sender{

  public static final int VERSION=1; /*add secure/no_secure version of smtp proto in ver.1*/

  public String getInfo(){
    return "Email sender executor";
  }
  public String getVersion(){
    return "1";
  }

  public String getStatus(String json){
    String status="";
    if(json!=null){
      JSONParser parser=new JSONParser();
      try{
        JSONObject jsonObject=(JSONObject)parser.parse(json);
        if(jsonObject!=null&&!jsonObject.isEmpty()){
          status=(String)jsonObject.get("status");
        }
      }catch(ParseException pe){}
    }
    return status;
  }
  public String getMessage(String json){
    String message="";
    if(json!=null){
      JSONParser parser=new JSONParser();
      try{
        JSONObject jsonObject=(JSONObject)parser.parse(json);
        if(jsonObject!=null&&!jsonObject.isEmpty()){
          JSONArray jsonArray=(JSONArray)jsonObject.get("results");
          if(jsonArray!=null&&jsonArray.size()>0){
            JSONObject jsonArray1Object=(JSONObject)jsonArray.get(0);//first
            if(jsonArray1Object!=null){
              message=(String)jsonArray1Object.get("message");
            }
          }
        }
      }catch(ParseException pe){}
    }
    return message;
  }

  public static String sendMessage(boolean is_secure,String server,String login,String password,String from,String to,String subject,String text){
    String ret_val="{\"results\":[{\"message\":\"Email has been sent.\"}],\"status\":\"SUCCESS\"}",error_message=null;
    try{
      Properties props = System.getProperties();

      int index = server.indexOf(":");
      if(index>-1){//server = hostname : port
        String port = server.substring(index+1);
        server = server.substring(0,index);
        if(is_secure)props.put("mail.smtps.port", port);
        else props.put("mail.smtp.port", port);
      }

      if(is_secure)props.put("mail.smtps.host", server);
      else props.put("mail.smtp.host", server);

      if(is_secure)props.put("mail.smtps.auth", "true");
      else props.put("mail.smtp.auth", "true");

      Session session = Session.getInstance(props, null);
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(from));

      InternetAddress[] addrs = InternetAddress.parse(to, false);
      msg.setRecipients(Message.RecipientType.TO, addrs);

      msg.setSubject(subject);
      msg.setText(text);
      msg.setSentDate(new Date());

      SMTPTransport t = (SMTPTransport) (is_secure?session.getTransport("smtps"):session.getTransport("smtp"));
      t.connect(server, login, password);
      t.sendMessage(msg, msg.getAllRecipients());
      t.close();
    }catch(Exception e){error_message=e.getLocalizedMessage();}

    if(error_message!=null)ret_val="{\"results\":[{\"message\":\""+error_message+"\"}],\"status\":\"ERROR\"}";

    return ret_val;
  }

  public static String sendMessage(String server,String login,String password,String from,String to,String subject,String text){
    return sendMessage(true,server,login,password,from,to,subject,text);
  }
  public static String sendSmtpMessage(String server,String login,String password,String from,String to,String subject,String text){
    return sendMessage(false,server,login,password,from,to,subject,text);
  }
  public static String sendSmtpsMessage(String server,String login,String password,String from,String to,String subject,String text){
    return sendMessage(true,server,login,password,from,to,subject,text);
  }
}