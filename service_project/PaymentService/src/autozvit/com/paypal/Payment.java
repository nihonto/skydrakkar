package autozvit.com.paypal;
import com.paypal.api.payments.Event;
import com.paypal.base.Constants;
import com.paypal.base.rest.APIContext;
//import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/*PayPal webbhooks parsing*/
//"custom":"{user_id=<number>,order_id=<number>}"
/*{"id":"WH-8AY125903L830745G-1CM79758AR1029745","event_version":"1.0","create_time":"2017-05-26T11:47:09.579Z","resource_type":"sale","event_type":"PAYMENT.SALE.COMPLETED","summary":"Payment completed for $ 231.14 USD","resource":{"id":"4B5591033A536641U","state":"completed","amount":{"total":"231.14","currency":"USD","details":{"subtotal":"231.14"}},"payment_mode":"INSTANT_TRANSFER","protection_eligibility":"ELIGIBLE","protection_eligibility_type":"ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE","transaction_fee":{"value":"7.00","currency":"USD"},"invoice_number":"","custom":"{user_id:1,order_id:3}","parent_payment":"PAY-0PE41646VM925841XLEUBLCY","create_time":"2017-05-26T11:46:43Z","update_time":"2017-05-26T11:46:43Z","links":[{"href":"https://api.sandbox.paypal.com/v1/payments/sale/4B5591033A536641U","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/payments/sale/4B5591033A536641U/refund","rel":"refund","method":"POST"},{"href":"https://api.sandbox.paypal.com/v1/payments/payment/PAY-0PE41646VM925841XLEUBLCY","rel":"parent_payment","method":"GET"}]},"links":[{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-8AY125903L830745G-1CM79758AR1029745","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-8AY125903L830745G-1CM79758AR1029745/resend","rel":"resend","method":"POST"}]}*/
//pay pal weebhook to backend server(need to parse "custom" field):
/*
{
  "id":"WH-6TG8981273521642K-90B46377SK948563R",
  "event_version":"1.0",
  "create_time":"2017-05-26T11:01:47.271Z",
  "resource_type":"sale",
  "event_type":"PAYMENT.SALE.COMPLETED",
  "summary":"Payment completed for $ 53.8 USD",
  "resource":{
    "id":"8N1439760W317741A",
    "state":"completed",
    "amount":{
      "total":"53.80",
      "currency":"USD",
      "details":{
        "subtotal":"53.80"
      }
    },
    "payment_mode":"INSTANT_TRANSFER",
    "protection_eligibility":"ELIGIBLE",
    "protection_eligibility_type":"ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE",
    "transaction_fee":{
      "value":"1.86",
      "currency":"USD"
    },
    "invoice_number":"",
    "custom":"{user_id:1,order_id:9}",
    "parent_payment":"PAY-3E633264HH974905FLEUAVKA",
    "create_time":"2017-05-26T11:00:48Z",
    "update_time":"2017-05-26T11:00:48Z",
    "links":[
    {
      "href":"https://api.sandbox.paypal.com/v1/payments/sale/8N1439760W317741A",
      "rel":"self",
      "method":"GET"
    },
    {
      "href":"https://api.sandbox.paypal.com/v1/payments/sale/8N1439760W317741A/refund",
      "rel":"refund",
      "method":"POST"
    },
    {
      "href":"https://api.sandbox.paypal.com/v1/payments/payment/PAY-3E633264HH974905FLEUAVKA",
      "rel":"parent_payment",
      "method":"GET"
    }]
  },
  "links":[
  {
    "href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-6TG8981273521642K-90B46377SK948563R",
    "rel":"self","method":"GET"
  },
  {
    "href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-6TG8981273521642K-90B46377SK948563R/resend",
    "rel":"resend","method":"POST"
  }]
}
*/
//"custom":"{user_id:6,order_id:39}"
public class Payment{
  public String getOrderId(String str){
    String s=getJsonString(str,1);
    return getValue(s,"order_id");
  }
  public String getUserId(String str){
    String s=getJsonString(str,1);
    return getValue(s,"user_id");
  }
  //"amount":{"total":"45.00","currency":"USD","details":{"subtotal":"45.00"}}
  public String getTotal(String str){
    return getJsonString(str,2);
  }
  public String getCurrency(String str){
    return getJsonString(str,3);
  }
  //type=1 custom, type=2 total, type=3 currency
  public String getJsonString(String json,int type){
    if(json!=null){
      JSONParser parser=new JSONParser();
      try{
        JSONObject jsonObject=(JSONObject)parser.parse(json);
        if(jsonObject!=null&&!jsonObject.isEmpty()){
          JSONObject resourceObject=(JSONObject)jsonObject.get("resource");
          if(resourceObject!=null&&!resourceObject.isEmpty()){
            if(type==1){
              return (String)resourceObject.get("custom");
            }
            else if(type==2||type==3){
              JSONObject amountObject=(JSONObject)resourceObject.get("amount");
              if(amountObject!=null&&!amountObject.isEmpty()){
                if(type==2)return (String)amountObject.get("total");
                else if(type==3)return (String)amountObject.get("currency");
              }
            }
          }
        }
      }catch(ParseException pe){}
    }
    return "";
  }
  private String getValue(String str,String name){
    int index;
    String token;
    StringTokenizer st=new StringTokenizer(str,"{,}");
    while(st.hasMoreTokens()){
      token=st.nextToken();
      index=token.indexOf(":");
      if(index!=-1){
        if(token.substring(0,index).trim().equals(name))
          return token.substring(index+1).trim();
      }
    }
    return "";
  }

  //1-signature verified ok 0-signature bad
  //header is the json string: {"param1"="value1","param2"="value2" ...}
  public String verifySignature(String client_id,String secret,String webhook_id,String mode,String header,String data){
    String ret_val="0";
    APIContext api_context=new APIContext(client_id,secret,mode);
    // Set the webhookId that you received when you created this webhook.
    api_context.addConfiguration(Constants.PAYPAL_WEBHOOK_ID,webhook_id);
    Boolean result=false;

    Map<String,String> map=new HashMap<String,String>();

    JSONParser parser=new JSONParser();
    try{
      JSONObject jsonObject=(JSONObject)parser.parse(header);
      if(jsonObject!=null&&!jsonObject.isEmpty()){
        Object[] param_list=jsonObject.keySet().toArray();
        Object[] value_list=jsonObject.entrySet().toArray();
        for(int i=0;i<param_list.length;i++){
          map.put((String)param_list[i],(String)value_list[i]);
        }
      }
    }catch(ParseException pe){}

    try{
      result=Event.validateReceivedEvent(api_context,map,data);
    }catch(Exception e){}
    if(result)ret_val="1";
    return ret_val;
  }
}
/*
verify_webhook_signature

https://developer.paypal.com/docs/api/webhooks/#verify-webhook-signature_post

auth_algo string
The algorithm that PayPal uses to generate the signature and that you can use to verify the signature. Extract this value from the PAYPAL-AUTH-ALGO response header, which is received with the webhook notification.
cert_url string
The X.509 public key certificate. Download the certificate from this URL and use it to verify the signature. Extract this value from the PAYPAL-CERT-URL response header, which is received with the webhook notification.
Format: uri.

transmission_id string
The ID of the HTTP transmission. Contained in the PAYPAL-TRANSMISSION-ID header of the notification message.
transmission_sig string
The PayPal-generated asymmetric signature. Contained in the PAYPAL-TRANSMISSION-SIG header of the notification message.
transmission_time string
The date and time of the HTTP transmission. Contained in the PAYPAL-TRANSMISSION-TIME header of the notification message.
Format: date-time.

webhook_id string
The ID of the webhook as configured in your Developer Portal account.
webhook_event object
The webhook notification. Contained in the HTTP POST request body.
*/