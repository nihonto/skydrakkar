package autozvit.com.googleplay;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.io.IOException;
public class Payment{
  private static final String KEY_FACTORY_ALGORITHM="RSA";
  private static final String SIGNATURE_ALGORITHM="SHA1withRSA";
  public String getOrderId(String callback_data){return getCallbackValue(callback_data,"orderId");}
  public String getProductId(String callback_data){return getCallbackValue(callback_data,"productId");}
  public String getPurchaseState(String callback_data){return getCallbackValue(callback_data,"purchaseState");}
  public String getPurchaseToken(String callback_data){return getCallbackValue(callback_data,"purchaseToken");}
  public String getPurchaseTime(String callback_data){return getCallbackValue(callback_data,"purchaseTime");}
  public String getDeveloperPayload(String callback_data){return getCallbackValue(callback_data,"developerPayload");}
  private String getCallbackValue(String json_data,String json_param){
    String ret_val="";
    try{
      int index,index1=json_data.indexOf(json_param),index2=(index1>=0?json_data.indexOf(":",index1):-1);
      if(index1>=0&&index2>=0){
        if(json_data.charAt(index2+1)=='\"'){index=index2+2;index1=json_data.indexOf("\"",index);}
        else{index=index2+1;index1=json_data.indexOf(",",index);}
        if(index1>=0)ret_val=json_data.substring(index,index1);
      }
    }catch(Exception e){}
    return ret_val;
  }
  public String parseCallback(String callback){
    String callback_data="";
    try{
      byte[] dec_data=new sun.misc.BASE64Decoder().decodeBuffer(callback);
      callback_data=new String(dec_data/*,"UTF-8"*/);
    }catch(Exception e){}
    return callback_data;
  }
  //1-signature verified ok 0-signature bad
  public String verifySignature(String base64PublicKey,String signedData,String signature){
    PublicKey key=null;
    try{
      byte[] decoded_key=new sun.misc.BASE64Decoder().decodeBuffer(base64PublicKey);
      KeyFactory key_factory=KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
      key=key_factory.generatePublic(new X509EncodedKeySpec(decoded_key));
    }catch(NoSuchAlgorithmException e){
    }catch(InvalidKeySpecException e){
    }catch(IOException e){}
    if(key!=null){
      Signature sig;
      try{
        sig=Signature.getInstance(SIGNATURE_ALGORITHM);
        sig.initVerify(key);
        sig.update(signedData.getBytes());
        if(!sig.verify(new sun.misc.BASE64Decoder().decodeBuffer(signature)))return "0";
        else return "1";
        }catch(NoSuchAlgorithmException e){
        }catch(InvalidKeyException e){
        }catch(SignatureException e){
        }catch(IOException e){}
    }
    return "0";
  }
}