package autozvit.com.stripe;
import com.stripe.Stripe;
import com.stripe.model.Charge;
/*import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.StripeException;*/
import java.util.HashMap;
public class Payment{
  public String getStatus(String callback_data){return getCallbackValue(callback_data,"status");}
  public String getMessage(String callback_data){return getCallbackValue(callback_data,"message");}
  private String getCallbackValue(String json_data,String json_param){
    String ret_val="";
    try{
      int index,index1=json_data.indexOf(json_param),index2=(index1>=0?json_data.indexOf(":",index1):-1);
      if(index1>=0&&index2>=0){                
        index=json_data.indexOf('\"',index2);
        if(index>=0){
          index1=json_data.indexOf('\"',index+1);
          if(index1>=0)ret_val=json_data.substring(index,index1);
        }
        else{
          index=index2+1;
          int ind1=json_data.indexOf(",",index),
              ind2=json_data.indexOf("}",index),
              ind3=json_data.indexOf("]",index);
          int next_ind=json_data.length()-1;
          if(ind1>=0&&ind1<next_ind)next_ind=ind1;
          if(ind2>=0&&ind2<next_ind)next_ind=ind2;
          if(ind3>=0&&ind3<next_ind)next_ind=ind3;
          ret_val=json_data.substring(index,next_ind);
        }
      }
    }catch(Exception e){}
    return ret_val;
  }
  //return json ({"name_1"="value_1", ... ,"name_N"="value_N"})
  public String createCharge(String key,String token,String amount,String currency,String description){
    String ret_val="{\"results\":[{\"message\":\"Payment successfuly done.\"}],\"status\":\"SUCCESS\"}";
    String api_key=key;
    Stripe.apiKey=api_key;
    java.util.Map<String,String> params=new HashMap<String,String>();
    //HashMap<String,Object> params=new HashMap<String,Object>();
    params.put("amount",amount);
    params.put("currency",currency);
    params.put("description",description);
    params.put("source",token);
    //If you are using Radar, consider passing any additional customer information and order information as metadata.
    //https://stripe.com/docs/charges
    /*Map<String,String> initialMetadata = new HashMap<String,String>();
    initialMetadata.put("order_id",order_id);
    params.put("metadata",initialMetadata);*/
    //try{Charge.create(params);}catch(Exception e){
    //  ret_val="{\"results\":[{\"message\":\""+e.getLocalizedMessage()+"\"}],\"status\":\"ERROR\"}";
    //}
    String error_message=null;
    Charge charge=null;
    /*try{
      charge=Charge.create(params);
    }catch(AuthenticationException au_e){error_message=au_e.getLocalizedMessage();}
     catch(InvalidRequestException ir_e){error_message=ir_e.getLocalizedMessage();}
     catch(APIConnectionException ac_e){error_message=ac_e.getLocalizedMessage();}
     catch(CardException c_e){error_message=c_e.getLocalizedMessage();}
     catch(APIException api_e){error_message=api_e.getLocalizedMessage();}
    catch(StripeException s_e){error_message=s_e.getLocalizedMessage();}*/
    //charge=new Charge();
    //charge.setId("test payment");

    if(error_message!=null)ret_val="{\"results\":[{\"message\":\""+error_message+"\"}],\"status\":\"ERROR\"}";
    else if(charge!=null)ret_val="{\"results\":[{\"message\":\"Payment successfuly done.\",\"id\":\""+charge.getId()+"\"}],\"status\":\"SUCCESS\"}";
    return ret_val;
  }
}