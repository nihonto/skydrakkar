package autozvit.com.liqpay;
import com.liqpay.LiqPay;
import java.net.URLDecoder;
import java.util.Map;
import java.util.HashMap;
public class Payment{
  public String getCallbackVersion(String callback_data){return getCallbackValue(callback_data,"version");}
  public String getCallbackPublicKey(String callback_data){return getCallbackValue(callback_data,"public_key");}
  public String getCallbackAmount(String callback_data){return getCallbackValue(callback_data,"amount");}
  public String getCallbackCurrency(String callback_data){return getCallbackValue(callback_data,"currency");}
  public String getCallbackDescription(String callback_data){return getCallbackValue(callback_data,"description");}
  public String getCallbackOrderId(String callback_data){return getCallbackValue(callback_data,"order_id");}
  public String getCallbackType(String callback_data){return getCallbackValue(callback_data,"type");}
  public String getCallbackTransactionId(String callback_data){return getCallbackValue(callback_data,"transaction_id");}
  public String getCallbackPaymentId(String callback_data){return getCallbackValue(callback_data,"payment_id");}
  public String getCallbackSenderPhone(String callback_data){return getCallbackValue(callback_data,"sender_phone");}
  public String getCallbackStatus(String callback_data){return getCallbackValue(callback_data,"status");}
  private String parseCallbackParamSignature(String str){
    String ret_val="";
    int index1=str.indexOf("signature="),index2=(index1>=0?str.indexOf("&",index1):-1);
    if(index1>=0){
      if(index2>=0)ret_val=str.substring(index1+10,index2);
      else ret_val=str.substring(index1+10);
    }
    return ret_val;
  }
  private String parseCallbackParamData(String str){
    String ret_val="";
    int index1=str.indexOf("data="),index2=(index1>=0?str.indexOf("&",index1):-1);
    if(index1>=0){
      if(index2>=0)ret_val=str.substring(index1+5,index2);
      else ret_val=str.substring(index1+5);
    }
    return ret_val;
  }
  private String getCallbackValue(String json_data,String json_param){
    String ret_val="";
    try{
      int index,index1=json_data.indexOf(json_param),index2=(index1>=0?json_data.indexOf(":",index1):-1);
      if(index1>=0&&index2>=0){
        if(json_data.charAt(index2+1)=='\"'){index=index2+2;index1=json_data.indexOf("\"",index);}
        else{index=index2+1;index1=json_data.indexOf(",",index);}
        if(index1>=0)ret_val=json_data.substring(index,index1);
      }
    }catch(Exception e){}
    return ret_val;
  }
  public String parseCallback(String public_key,String private_key,String callback){
    String ret_val="";
    try{
      String si=parseCallbackParamSignature(callback);
      String da=parseCallbackParamData(callback);
      String signature=URLDecoder.decode(si,"UTF-8");
      String data=URLDecoder.decode(da,"UTF-8");
      byte[] dec_data=new sun.misc.BASE64Decoder().decodeBuffer(data);
      String callback_data=new String(dec_data/*,"UTF-8"*/);
      if(signature.equals(getSignature(public_key,private_key,data)))ret_val=callback_data;
    }catch(Exception e){}
    return ret_val;
  }
  public String getSignature(String public_key,String private_key,String data){//return signature
    String ret_val="";
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      ret_val=liqpay.str_to_sign(private_key+data+private_key);
    }catch(Exception e){}
    return ret_val;
  }
  public String getSignature(String public_key,String private_key,String amount,String currency,String description,String order_id,String server_url,String result_url,String language,String sandbox){
    String ret_val="";
    HashMap params=new HashMap();
    params.put("version","3");
    params.put("amount",amount);
    params.put("currency",currency);
    params.put("description",description);
    params.put("order_id",order_id);
    params.put("server_url",server_url);
    params.put("result_url",result_url);
    params.put("language",language);
    if(sandbox.equals("1"))params.put("sandbox",sandbox);//test mode
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      //ret_val=liqpay.cnb_signature(params);
    }catch(Exception e){}
    return ret_val;
  }
  public String getHtmlForm(String public_key,String private_key,String amount,String currency,String description,String order_id,String server_url,String result_url,String language,String sandbox){
    String ret_val="";
    HashMap params=new HashMap();
    params.put("version","3");
    params.put("action","pay");
    params.put("amount",amount);
    params.put("currency",currency);
    params.put("description",description);
    params.put("order_id",order_id);
    params.put("server_url",server_url);
    params.put("result_url",result_url);
    params.put("language",language);
    if(sandbox.equals("1"))params.put("sandbox",sandbox);//test mode
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      ret_val=liqpay.cnb_form(params);
    }catch(Exception e){}
    return ret_val;
  }
  public String getStatus(String public_key,String private_key,String order_id){//return error|success
    String ret_val="{\"results\":[{\"message\":\"Invalid payment service\"}],\"status\":\"ERROR\"}";
    HashMap params=new HashMap();
    params.put("version","3");
    params.put("order_id",order_id);
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      Map res=liqpay.api("payment/status",params);
      if(res.get("result").equals("ok"))ret_val="{\"results\":[{\"status\":\""+(String)res.get("status")+"\"}],\"status\":\"SUCCESS\"}";
      else ret_val="{\"results\":[{\"message\":\"Invalid payment order="+res.get("result")+"\"}],\"status\":\"ERROR\"}";
    }catch(Exception e){}
    return ret_val;
  }
  //methods getPay, getVerify signed as private for special users of Liqpay(http://ru.pcisecuritystandards.org/)
  public String getPay(String public_key,String private_key,String phone,String amount,String currency,String description,String order_id,String card_number,String month,String year,String cvv,String server_url,String result_url,String sandbox){//return error|(otp_code)
    String ret_val="{\"results\":[{\"message\":\"Invalid payment service\"}],\"status\":\"ERROR\"}";
    HashMap params=new HashMap();
    params.put("version","3");
    params.put("phone",phone);
    params.put("amount",amount);
    params.put("currency",currency);
    params.put("description",description);
    params.put("order_id",order_id);
    params.put("card",card_number);
    params.put("card_exp_month",month);
    params.put("card_exp_year",year);
    params.put("card_cvv",cvv);
    params.put("server_url",server_url);
    params.put("result_url",result_url);
    if(sandbox.equals("1"))params.put("sandbox",sandbox);//test mode
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      Map res=liqpay.api("payment/pay",params);
      if(res.get("result").equals("ok"))ret_val="{\"results\":[{\"token\":\""+(String)res.get("token")+"\"}],\"status\":\"SUCCESS\"}";
      else ret_val="{\"results\":[{\"message\":\"Payment is not processed="+res.get("result")+"\"}],\"status\":\"ERROR\"}";
    }catch(Exception e){}
    return ret_val;
  }
  public String getVerify(String public_key,String private_key,String token,String otp){//return error|(redirect_to|payment_id)
    String ret_val="{\"results\":[{\"message\":\"Invalid payment service\"}],\"status\":\"ERROR\"}";
    HashMap params = new HashMap();
    params.put("version","3");
    params.put("token",token);
    params.put("otp",otp);
    try{
      LiqPay liqpay=new LiqPay(public_key,private_key);
      Map res=liqpay.api("payment/verify",params);
      if(res.get("result").equals("ok")){
        String status=(String)res.get("status");
        if(status.equals("3ds_verify"))ret_val="{\"results\":[{\"redirect_to\":\""+(String)res.get("redirect_to")+"\"}],\"status\":\"SUCCESS\"}";
        else if(status.equals("success"))ret_val="{\"results\":[{\"payment_id\":\""+(String)res.get("payment_id")+"\"}],\"status\":\"SUCCESS\"}";
        else ret_val="{\"results\":[{\"message\":\"Payment runtime error="+status+"\"}],\"status\":\"ERROR\"}";
      }
      else ret_val="{\"results\":[{\"message\":\"Payment verify failed="+res.get("result")+"\"}],\"status\":\"ERROR\"}";
    }catch(Exception e){}
    return ret_val;
  }
}