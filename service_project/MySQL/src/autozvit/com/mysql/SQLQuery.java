package autozvit.com.mysql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
/*Executor of sql query list separated by delimiter = ";"*/
public class SQLQuery{
  public String getInfo(){
    return "MySQL sql queries executor";
  }
  public String getVersion(){
    return "1.0";
  }
  public String start(String username,String password,String database,String sql_query_list){
    String ret_val="Error";
    Connection connection=null;
    Statement stmt=null;
    String driver="com.mysql.jdbc.Driver",jdbc_mysql="jdbc:mysql://localhost:3306/";//jdbc:mysql://
    String sql;
    try{
    try{
      Class.forName(driver);
      connection=DriverManager.getConnection(jdbc_mysql+database,username,password);
      StringTokenizer st=new StringTokenizer(sql_query_list,";");
      while(st.hasMoreTokens()){
        stmt=connection.createStatement();
        sql=st.nextToken();
        if(sql!=null&&sql.length()>0)stmt.execute(sql);
      }
      ret_val="Success";
    }catch(SQLException sql_e){ret_val=sql_e.toString();}
    finally{
      try{if(stmt!=null)stmt.close();stmt=null;}catch(SQLException sql_e){}
      if(connection!=null){connection.close();connection=null;}
    }
    }catch(Exception e){ret_val=e.toString();}
    return ret_val;
  }
}