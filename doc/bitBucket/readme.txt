setup Git
---------

mkdir /path/to/your/project
cd /path/to/your/project
git init
git remote add origin https://nihonto@bitbucket.org/nihonto/skydrakkar.git

push
----

git push -u origin master
