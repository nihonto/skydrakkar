Use java function String.format(...)

example

String.format("string message=%s","message for substitution");

or

String.format("more than one subst string message=%1$ next message=%2$ last message=%3$","message for substitution","next message","last message");
String.format("more than one subst string message=%s next message=%s last message=%s","message for substitution","next message","last message");




Alternative, in shablon(template) write substitution

<table id="table_class" name="table_name" ... format="<tr><td>%s</td></tr>">
@format (place of format)
</table>

<a href="%" title="%s" ... format="%s">
@format (place of format)
</a>

<format="<a href='%s' title='%s'>%s</a>">
</format>